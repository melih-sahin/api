docker-compose build && docker-compose up -d
cp ./src/.env.example ./src/.env
docker-compose exec php composer install
docker-compose exec php /var/www/html/artisan config:clear
docker-compose exec php /var/www/html/artisan key:generate
docker-compose exec php /var/www/html/artisan migrate:fresh --seed
docker-compose exec php /var/www/html/artisan elastic:migrate
docker-compose exec php /var/www/html/artisan scout:import "App\Models\Document"


