<?php

namespace Database\Factories;

use App\Models\Document;
use Illuminate\Database\Eloquent\Factories\Factory;

class DocumentFactory extends Factory
{
    protected $model = Document::class;

    public function definition(): array
    {
        return [
            'author' => $this->faker->firstName,
            'title' => $this->faker->sentence,
            'content' => $this->faker->paragraph,
        ];
    }
}
