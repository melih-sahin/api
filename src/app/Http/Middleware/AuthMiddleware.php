<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class AuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->input('token')) {
            if ($request->input('token') === Cache::get('token')) {
                return $next($request);
            }
            return response()->json([
                'code' => 401,
                'message' => 'Unauthorized'
            ]);
        }
        return $next($request);
    }
}
