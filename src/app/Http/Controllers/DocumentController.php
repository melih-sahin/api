<?php

namespace App\Http\Controllers;

use App\Models\Document;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class DocumentController extends Controller
{
    public function show($id)
    {
        return Document::where('id', $id)->get();
    }

    public function search(Request $request): JsonResponse
    {
        if ($request->has('query_params')) {
            $param = $request->input('query_params');
            return response()->json([
                'status' => 200,
                'results' => Document::search($param)->get(),
            ]);
        }
    }
}
