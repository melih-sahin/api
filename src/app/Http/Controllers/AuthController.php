<?php

namespace App\Http\Controllers;

use App\Http\Requests\AuthRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function login(AuthRequest $request): JsonResponse
    {
        $credentials = $request->validated();

        if (!Auth::attempt($credentials)) {
            return response()->json([
                'code' => 401,
                'message' => 'Unauthorized'
            ]);
        }
        $token = Hash::make(bin2hex(random_bytes(20)));
        Cache::store('redis')->put('token', $token, 300);

        return response()->json([
            'status' => 200,
            'token' => $token,
        ]);
    }
}
