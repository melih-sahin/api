<?php

namespace App\Models;

use Database\Factories\DocumentFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Document extends Model
{
    use HasFactory, Searchable;

    protected $fillable = [
        'author',
        'title',
        'content',
    ];

    protected static function newFactory(): DocumentFactory
    {
        return DocumentFactory::new();
    }

    public function searchableAs(): string
    {
        return 'documents_index';
    }

    public function toSearchableArray(): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'author' => $this->author,
        ];
    }
}
