
## API

## Api document
- https://documenter.getpostman.com/view/3096316/TWDWJccb#a2c7b081-b63b-4fb1-810f-7e07c9c93faf

## Api collection
- https://drive.google.com/file/d/19fvhljElih_YJEfHNZyYSA8L4g3UoSLm/view

## All Endpoints
```
 POST | api/login                                      
 GET  | api/document?{query_params: #keyword #id}   
 GET  | api/document/{id}                                                                                                                      
```

### Getting Started

### Installation (Manual)
```
$ git clone git@bitbucket.org:melih-sahin/api.git
$ bash run.sh
```
